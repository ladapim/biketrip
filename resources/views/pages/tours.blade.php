@extends('layouts.app')

@section('content')
    <h1>{{$title}}</h1>
    @if(count($tours) > 0)
        <ul class="list-group">
            @foreach($tours as $tour)
                <li class="list-group-item">{{$tour}}</li>
            @endforeach
        </ul>
    @endif
@endsection
